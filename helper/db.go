package helper

import (
	"fmt"
	"log"
	"reflect"

	db "github.com/eaciit/dbox"
	_ "github.com/eaciit/dbox/dbc/mongo"
	"github.com/eaciit/orm/v1"
	tk "github.com/eaciit/toolkit"
)

// ConnectionParams : Struct for defining database connection parameters
type ConnectionParams struct {
	Host, Database, Username, Password, Driver string
}

// GetConnection : Connecting to database using input params or using defined configuration file
func GetConnection(connectionParams ...ConnectionParams) (db.IConnection, error) {
	var cp ConnectionParams

	if len(connectionParams) > 0 {
		cp = connectionParams[0]
	} else {
		config := ReadConfig()
		cp.Host = config.GetString("host")
		cp.Database = config.GetString("database")
		cp.Password = config.GetString("password")
		cp.Driver = config.GetString("driver")
	}

	log.Println("Connecting database", cp.Host, "...")

	ci := &db.ConnectionInfo{
		Host:     cp.Host,
		Database: cp.Database,
		UserName: cp.Username,
		Password: cp.Password,
		Settings: tk.M{}.Set("timeout", 10.0),
	}

	c, e := db.NewConnection(cp.Driver, ci)

	if e != nil {
		return nil, e
	}

	e = c.Connect()
	if e != nil {
		return nil, e
	}

	log.Println("Database successfully connected")

	return c, nil
}

// Get : Simple table fetcher that is able to handle server paging, sorting, and filtering
func Get(model orm.IModel, params tk.M, filter *db.Filter, ctx ...*orm.DataContext) (interface{}, int, error) {
	var conn *orm.DataContext
	var e error

	if len(ctx) > 0 {
		conn = ctx[0]
	} else {
		c, e := GetConnection()
		conn = orm.New(c)
		if e != nil {
			log.Fatal(e)
			return nil, 0, e
		}
		defer conn.Close()
	}

	q := conn.Connection.NewQuery().From(model.TableName())

	if params != nil {
		// Applying limit
		if val, ok := params["take"]; ok {
			q.Take(int(val.(float64)))
		}

		// Applying offset
		if val, ok := params["skip"]; ok {
			q.Skip(int(val.(float64)))
		}

		// Applying sorting
		if val, ok := params["sort"]; ok {
			var fields []string
			for _, sort := range val.([]interface{}) {
				s := sort.(map[string]interface{})
				if s["dir"] == "desc" {
					fields = append(fields, fmt.Sprint("-", s["field"].(string)))
				} else {
					fields = append(fields, s["field"].(string))
				}
			}
			q.Order(fields...)
		}
	}

	// Adding filter parameters
	if filter != nil {
		q.Where(filter)
	}

	// Fetching Data
	cursor, e := q.Cursor(nil)
	if e != nil {
		log.Fatal(e)
		return nil, 0, e
	}
	defer cursor.Close()

	modelType := reflect.TypeOf(model).Elem()
	pointer := reflect.New(reflect.SliceOf(modelType)).Interface()
	e = cursor.Fetch(pointer, 0, false)

	if e != nil {
		log.Fatal(e)
		return nil, 0, e
	}

	data := reflect.ValueOf(pointer).Elem().Interface()

	return data, cursor.Count(), nil
}
