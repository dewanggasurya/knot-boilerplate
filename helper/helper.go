package helper

import (
	"os"
	"reflect"

	"github.com/eaciit/orm/v1"
)

var (
	// Controllers : Variable containing all defined controllers
	Controllers []interface{}
)

// PrepareDirectory : Create directory when it is not exists
func PrepareDirectory(path string) string {
	mode := int(0777)
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.MkdirAll(path, os.FileMode(mode))
	}

	return path
}

// ModelToJS : Generate golang model to javascript model
func ModelToJS(model orm.IModel) {
	modelType := reflect.TypeOf(model)

	for i := 0; i < modelType.NumField(); i++ {
		modelType.Field(i)
	}
}
