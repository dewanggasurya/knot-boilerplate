package helper

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	tk "github.com/eaciit/toolkit"
)

/*
** configPathGlobal : Used for storing string path of config that is defined config path from option or from another source
** configCache 		: Used for caching config file. So the file reading process could be minimalized
**/
var (
	configPathGlobal string
	configCache      tk.M = tk.M{}
)

func ReadConfig(configPath ...string) tk.M {
	var configFile string

	if len(configPathGlobal) > 0 {
		configFile = configPathGlobal
	} else {
		if len(configPath) > 0 {
			configFile = configPath[0]
		} else {
			configFile = GetConfigPath()
		}
		configPathGlobal = configFile
	}

	if configCache.Has(configFile) {
		return configCache.Get(configFile).(tk.M)
	}

	config := make(tk.M)

	if _, err := os.Stat(configFile); os.IsNotExist(err) {
		log.Fatal("File config (", configFile, ") does not exists")
		return nil
	}

	b, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Fatal(err.Error())
		return nil
	}

	for _, line := range strings.Split(string(b), "\r\n") {
		token := strings.Split(line, "=")
		if len(token) > 0 {
			config.Set(token[0], strings.Replace(
				line,
				token[0]+"=",
				"",
				1,
			))
		}
	}

	configCache.Set(configFile, config)

	return config
}

func GetConfigPath() string {
	dir, _ := os.Getwd()
	configDir := filepath.Join(dir, "apps", "config", "app.conf")
	return configDir
}

func ResetConfigCache() {
	configCache = tk.M{}
}
