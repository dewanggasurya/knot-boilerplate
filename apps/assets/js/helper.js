helper.kendo = {};
helper.kendo.grid = {};
helper.const = {};

helper.const.ContentTypeJSON = "application/json; charset=utf-8";

helper.kendo.grid.refresh = function(element, callback){

    if(!element){
        return -1;
    }

    var grid = $(element).getKendoGrid();    
    if(!!grid){
        var dataSource = grid.dataSource;

        if(!!dataSource) {
            dataSource.fetch().done(function(r){
                grid.refresh();

                if(!!callback) callback(r);
            });
        }
    }
};

helper.kendo.grid.destroy = function(element){
    var grid =  $(element).getKendoGrid();
   
    if(typeof grid !== "undefined"){
        grid.destroy();
    }

    $(element).empty();
};