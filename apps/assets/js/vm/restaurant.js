var restaurant = {};

restaurant.render = {};
restaurant.model = ko.observable({});

restaurant.add = function(){
    $.ajax({
        url : "/restaurant/get",
        method : "POST",
        data : {
            "testing" : 1
        },
        success : function(data){
            console.log("data", data);
        },
        error : function(xhr, status, error){
            console.log("error", error);
        }
    });
};

restaurant.render.grid = function(){
    var element = $("#grid");        
    helper.kendo.grid.destroy("#grid");        

    element.kendoGrid({
        dataSource : {
            transport : {
                read : {
                    type : "POST",
                    url : "/restaurant/get",                                        
                    contentType: helper.const.ContentTypeJSON,
                    dataType: "json",
                },
                parameterMap: function (data) {
                    return kendo.stringify(data);
                }
            },
            schema : {
                data : "Data.Rows",                    
                total : "Data.Total",                    
            },
            pageSize: 15,
            serverPaging : true,
            serverSorting : true, 
        },
        columns : [
            { field : "borough", title : "Borough" },
            { field : "cuisine", title : "Cuisine" },
            { field : "name", title : "Name" },
        ],
        pageable : true,
        sortable : true
    });
};