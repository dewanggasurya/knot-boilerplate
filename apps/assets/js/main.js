/* Defining Attributes */
vm.title = ko.observable("Loading");
vm.headline = ko.observable("...");

/* Defining Methods */
vm.initLayout = function(){            
    var sidebar = $("#sidebar");
    var content = $("#content");
    var sidebarHeight = sidebar.height();
    var height = content.height() + 80;
    
    // Setting sidebar height to match window height
    sidebar.css({
        "min-height" : height
    });
    
    window.onresize = function(e){
        vm.initLayout();
    };
};

vm.toggleSidebar = function(){    
    var sidebar = $("#sidebar");
    var overlay = $("#sidebar-overlay");
    var btnToggle = $("#toggle-sidebar");

    if(sidebar.is(".expanded")){        
        sidebar.animateCSS("slideOutLeft", function(){
            sidebar.removeClass('expanded');            
            sidebar.animateCSS("slideInLeft");            
            btnToggle.css("visibility", "hidden");
            overlay.fadeOut();
        });                  
    }else{
        sidebar.animateCSS("slideOutLeft", function(){
            sidebar.addClass('expanded');
            sidebar.animateCSS("slideInLeft");                        
            btnToggle.css("visibility", "visible");
            overlay.fadeIn();
        });        
    }    
};