package models

import (
	"log"
	"sandbox/helper"

	"gopkg.in/mgo.v2/bson"

	"github.com/eaciit/orm/v1"
	tk "github.com/eaciit/toolkit"
)

var (
	conn *orm.DataContext
)

type Restaurant struct {
	orm.ModelBase `json:"-" bson:"-"`
	ID            bson.ObjectId `json:"_id" bson:"_id"`
	Address       Address       `json:"address" bson:"address"`
	Borough       string        `json:"borough" bson:"borough"`
	Cuisine       string        `json:"cuisine" bson:"cuisine"`
	Grades        []Grade       `json:"grades" bson:"grades"`
	Name          string        `json:"name" bson:"name"`
	RestaurantID  string        `json:"restaurant_id" bson:"restaurant_id"`
}

func NewRestaurant(ctx *orm.DataContext) *Restaurant {
	conn = ctx
	m := new(Restaurant)
	m.ID = bson.NewObjectId()
	return m
}

func (m *Restaurant) RecordID() interface{} {
	return m.ID
}

func (m *Restaurant) TableName() string {
	return "Restaurant"
}

func (m *Restaurant) Get(params ...tk.M) ([]Restaurant, int, error) {
	param := tk.M{}
	if len(params) > 0 {
		param = params[0]
	} else {
		param = nil
	}

	data, total, e := helper.Get(m, param, nil, conn)
	if e != nil {
		log.Fatal(e)
		return nil, 0, e
	}

	return data.([]Restaurant), total, e

}
