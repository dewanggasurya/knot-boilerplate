package models

import "github.com/eaciit/orm/v1"

type Grade struct {
	orm.ModelBase `json:"-"`
	Date          string `json:"date"`
	Grade         string `json:"grade"`
	Score         string `json:"score"`
}
