package models

import "github.com/eaciit/orm/v1"

type Address struct {
	orm.ModelBase `json:"-"`
	Building      string `json:"building"`
	Coordinate    string `json:"coord"`
	Street        string `json:"street"`
	ZipCode       string `json:"zipcode"`
}
