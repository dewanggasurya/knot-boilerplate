package apps

import (
	"flag"
	"log"
	"path/filepath"

	"github.com/eaciit/knot/knot.v1"
	"github.com/eaciit/orm/v1"

	. "sandbox/apps/controllers"
	"sandbox/helper"
)

func init() {
	log.Println("<<< INIT START >>>")

	// Defining command options and default value
	configPath := flag.String("config", "./apps/config/app.conf", "Path of the config file")
	flag.Parse()

	// Reading config file
	cfg := helper.ReadConfig(*configPath)

	// Connecting to database
	cp := helper.ConnectionParams{
		Host:     cfg.GetString("host"),
		Database: cfg.GetString("database"),
		Username: cfg.GetString("username"),
		Password: cfg.GetString("password"),
		Driver:   cfg.GetString("driver"),
	}

	conn, err := helper.GetConnection(cp)

	if err != nil {
		log.Fatal(err)
	}

	// Declaring BaseController
	baseCtrl := new(BaseController)
	baseCtrl.Ctx = orm.New(conn)
	baseCtrl.DbHost = cp.Host
	baseCtrl.DbName = cp.Database
	baseCtrl.DbPassword = cp.Password
	baseCtrl.DbUsername = cp.Username

	// Defining app parameter
	app := knot.NewApp(cfg.GetString("appName"))
	app.LayoutTemplate = "_layout.html"
	app.ViewsPath = filepath.Join(
		cfg.GetString("basePath"),
		cfg.GetString("appDir"),
		cfg.GetString("viewsDir"),
	)
	app.Static("static", filepath.Join(
		cfg.GetString("basePath"),
		cfg.GetString("appDir"),
		cfg.GetString("assetsDir"),
	))

	// Registering Controller Routes
	allControllers := []interface{}{
		&(DashboardController{BaseController: baseCtrl}),
		&(RestaurantController{BaseController: baseCtrl}),
	}

	helper.Controllers = allControllers
	for _, c := range allControllers {
		app.Register(c)
	}

	knot.RegisterApp(app)
	log.Println("<<< INIT FINISH >>>")
}
