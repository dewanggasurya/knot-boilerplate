package controllers

import (
	"sandbox/apps/models"
	"sandbox/helper"

	"github.com/eaciit/knot/knot.v1"
	tk "github.com/eaciit/toolkit"
)

// RestaurantController : restaurant/
type RestaurantController struct {
	*BaseController
}

// Index : restaurant/index
func (c *RestaurantController) Index(k *knot.WebContext) interface{} {
	model := models.NewRestaurant(c.Ctx)
	helper.ModelToJS(model)
	c.SetOutputHtml(k, "Restaurant | Index")
	return c.GetViewData(k, nil)
}

// Get : restaurant/get
func (c *RestaurantController) Get(k *knot.WebContext) interface{} {
	r := tk.NewResult()
	c.SetOutputJSON(k)

	params := tk.M{}
	e := k.GetPayload(&params)

	if e != nil {
		return tk.NewResult().SetError(e)
	}
	model := models.NewRestaurant(c.Ctx)
	data, total, e := model.Get(params)

	if e != nil {
		return r.SetError(e)
	}

	return r.SetData(tk.M{"Total": total, "Rows": data})

}
