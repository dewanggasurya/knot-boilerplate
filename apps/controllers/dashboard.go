package controllers

import (
	"github.com/eaciit/knot/knot.v1"
)

type DashboardController struct {
	*BaseController
}

func (c *DashboardController) Index(k *knot.WebContext) interface{} {
	c.SetOutputHtml(k, "Dashboard | Index")
	return c.GetViewData(k, nil)
}
