package controllers

import (
	"sandbox/helper"

	"github.com/eaciit/knot/knot.v1"
	"github.com/eaciit/orm/v1"
	tk "github.com/eaciit/toolkit"
)

type IBaseController interface{}

type BaseController struct {
	base       IBaseController
	Ctx        *orm.DataContext `json:"ctx"`
	Page       PageInfo         `json:"page"`
	DbName     string           `json:"dbName"`
	DbHost     string           `json:"dbHost"`
	DbUsername string           `json:"dbUsername"`
	DbPassword string           `json:"dbPassword"`
	BasePath   string           `json:"basePath"`
	UploadPath string           `json:"uploadPath"`
}

type PageInfo struct {
	Title       string            `json:"title"`
	Breadcrumbs map[string]string `json:"breadcrumbs"`
}

type Privilege struct {
	View          bool     `json:"view"`
	Create        bool     `json:"create"`
	Edit          bool     `json:"edit"`
	Delete        bool     `json:"delete"`
	Approve       bool     `json:"approve"`
	Process       bool     `json:"process"`
	MenuID        string   `json:"menuID"`
	MenuName      string   `json:"menuName"`
	Username      string   `json:"username"`
	TopMenu       string   `json:"topMenu"`
	Roles         string   `json:"roles"`
	ListRegion    []string `json:"listRegion"`
	ListCountry   []string `json:"listCountry"`
	SelRegion     string   `json:"selRegion"`
	SelCountry    string   `json:"selCountry"`
	EnableRegion  string   `json:"enableRegion"`
	EnableCountry string   `json:"enableCountry"`
}

func (b *BaseController) SetOutputHtml(k *knot.WebContext, title string, breadcrumbs ...map[string]string) {
	k.Config.NoLog = true
	k.Config.OutputType = knot.OutputTemplate

	b.Page.Title = title
	b.Page.Breadcrumbs = nil
	if len(breadcrumbs) > 0 {
		b.Page.Breadcrumbs = breadcrumbs[0]
	}
}

func (b *BaseController) SetOutputJSON(k *knot.WebContext) {
	k.Config.NoLog = true
	k.Config.OutputType = knot.OutputJson
}

func (b *BaseController) GetViewData(k *knot.WebContext, data tk.M) tk.M {
	if data == nil {
		data = tk.M{}
	}

	// Basic Info
	cfg := helper.ReadConfig()
	data.Set("AppName", cfg.Get("AppName"))

	// Page info
	data.Set("Breadcrumb", b.Page.Breadcrumbs)
	data.Set("Title", cfg.Get("AppName"))
	if len(b.Page.Title) > 0 {
		data.Set("Title", b.Page.Title)
	}

	// Session Info
	// Not implemented yet

	return data
}
