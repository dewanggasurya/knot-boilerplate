package main

import (
	"fmt"
	"log"
	"net/http"
	p "net/http/pprof"
	_ "sandbox/apps"
	"sandbox/helper"

	knot "github.com/eaciit/knot/knot.v1"
)

func main() {
	// Fetching configuration
	cfg := helper.ReadConfig()

	// Setting up knot
	app := knot.GetApp(cfg.GetString("appName"))
	if app == nil {
		log.Fatal("App not found")
		return
	}

	// Defining basic routes
	routes := map[string]knot.FnContent{
		"/debug/pprof/": func(k *knot.WebContext) interface{} {
			p.Index(k.Writer, k.Request)
			return ""
		},
		"/debug/pprof/cmdline": func(k *knot.WebContext) interface{} {
			p.Cmdline(k.Writer, k.Request)
			return ""
		},
		"/debug/pprof/profile": func(k *knot.WebContext) interface{} {
			p.Profile(k.Writer, k.Request)
			return ""
		},
		"/debug/pprof/symbol": func(k *knot.WebContext) interface{} {
			p.Symbol(k.Writer, k.Request)
			return ""
		},
		"/debug/pprof/trace": func(k *knot.WebContext) interface{} {
			p.Trace(k.Writer, k.Request)
			return ""
		},
		"/": func(k *knot.WebContext) interface{} {
			http.Redirect(k.Writer, k.Request, "/dashboard/index", http.StatusTemporaryRedirect)
			return true
		},
	}

	// Preparing parameter
	server := fmt.Sprintf("%s:%s", cfg.GetString("runServer"), cfg.GetString("runPort"))

	// Starting App Server
	knot.StartAppWithFn(app, server, routes)
}
