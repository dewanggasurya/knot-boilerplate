# README #

Wannabe professional backend and frontend template development using [Knot](https://github.com/eaciit/knot) framework that is developed by [EACIIT](eaciit.com)

### How to use it ###
```go get -u bitbucket.org/dewanggasurya/knot-boilerplate```

### Who do I talk to? ###

* Mail : [dewangga.surya@gmail.com](mailto:dewangga.surya@gmail.com)